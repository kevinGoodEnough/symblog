# Symblog (Symfony blog)

Creation d'un blog d'entrainement pour Symfony.

## Running

Nécessite les technologies suivante pour fonctionner.
  * PHP 7.3
  * Composer
  * MySQL 8.0
   
A partir de la racine du projet ,tapez les commandes suivantes. 

1. Installation des dependances.
  
```php 
composer install
```


2. Créez une copie du fichier *.Env.* et le renommé **.env.local** .

3. Dans le fichier *env.local*, ajoutez les identifiants de connexions et le nom de la base de donnée à créer dans la ligne:
 
```
DATABASE_URL=mysql://ID:PASSWORD@127.0.0.1:3306/NomBDD
```

4. Assurez-vous d'avoir activer l'extension pdo_mysql du fichier php.ini
'''
extension=pdo_mysql
'''

5. Créez la base de donnée et ses tables.

```php
php bin/console doctrine:database:create
```

```php
php bin/console doctrine:migrations:migrate
```


6. Utilisez Faker pour remplir la Base de donnée de contenue.

```php
php bin/console doctrine:fixtures:load
```


7. Lancez le serveur php.
 
```php
php -S localhost:8000 -t public
```


7. Connectez vous.

```php
 localhost:8000
```

