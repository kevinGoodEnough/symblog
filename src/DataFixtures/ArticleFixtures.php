<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Comment;
 

class ArticleFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $fakerVariable = \Faker\Factory::create('fr_FR');

        // Créer 3 catégories fakées
        for($i = 1; $i <=3; $i++) {
            $category = new Category();
            $category->setTitle($fakerVariable->sentence())
                     ->setDescription($fakerVariable->paragraph());

            $manager->persist($category); 
        

            // Créer entre 4 et 6 articles
            for($j = 1; $j <= mt_rand(4, 6); $j++){
                $article = new Article();

                $content = '<p>' . join($fakerVariable->paragraphs(5), '</p><p>') . '</p>';

                $article = new Article();
                $article->setTitle($fakerVariable->sentence())
                        ->setContent($content)
                        ->setImage($fakerVariable->imageUrl())
                        ->setCreatedAt($fakerVariable->dateTimeBetween('-6 months'))
                        ->setCategory($category);

                $manager->persist($article);
                
                // On donne des commentaires à l'article
                for($k = 1; $k <= mt_rand(4, 10); $k++) {
                    $comment = new Comment();

                    $content = '<p>' . join($fakerVariable->paragraphs(2), '</p><p>') . '</p>';

                    $days = (new \DateTime())->diff($article->getCreatedAt())->days;

                    $comment->setAuthor($fakerVariable->name)
                        ->setContent($content)
                        ->setCreatedAt($fakerVariable->dateTimeBetween('-' . $days . ' days' )) 
                        ->setArticle($article);    
                        
                    $manager->persist($comment);
                }
            }
        }

        $manager->flush();
    }
}
