<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use App\Entity\Comment;
use App\Form\CommentType;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BlogController extends AbstractController
{

    /**
     * @Route("/", name="home")
     */
    public function home(){
        return $this->render('blog/home.html.twig');
    }

    /**
     * @Route("/blog", name="blog_articles")
     */
    public function index( ArticleRepository $repo){
        //Je demande à doctrine d'aller chercher le repository de la class/table "Article"
        $repo = $this->getDoctrine()->getRepository(Article::class);

        $articles = $repo->findDesc();

        return $this->render('blog/index.html.twig', [
            'controller_name' => 'BlogController',
            'articles' => $articles
            ]);
    }

    /**
    * @Route("/blog/new", name="blog_create")
    * @Route("/blog/edit/{id}", name="blog_edit")
    *
    * Require ROLE_ADMIN for only this controller method.
    * @IsGranted("ROLE_USER")
    */
   public function formArticle(Article $article = null, Request $request, ObjectManager $manager){

        if(!$article) {
            $article = new Article();
        }

        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            if(!$article->getid()){
                $article->setCreatedAt(new \DateTime());
            }

            $manager->persist($article);
            $manager->flush();

            return $this->redirectToRoute('blog_show', ['id' => $article->getId()]);
        }

        return $this->render('blog/create.html.twig', [
            'formArticle' => $form->createView(),
            'editMode' => $article->getId() !== null
        ]);
   }

    /**
     * @Route("/blog/{id}", name="blog_show")
     */
     public function show($id, Request $request,ObjectManager $manager, Article $article)
        {

            $comment = new Comment();

            $formComment = $this->createForm(CommentType::class, $comment);
            $formComment->handleRequest($request);


            if($formComment->isSubmitted() && $formComment->isValid()) {
                $comment->setCreatedAt(new \DateTime())
                        ->setArticle($article);


                $manager->persist($comment);
                $manager->flush();
            }
            
                return $this->render('blog/show.html.twig', [
                    'article' => $article,
                    'formComment' => $formComment->createView()
                ]);

        }

    /**
     * @Route("/blog/delete/{id}", name="blog_delete")
     * Require ROLE_ADMIN for only this controller method.
     * @IsGranted("ROLE_USER")
     */
    public function delete(Article $article, ObjectManager $manager, ArticleRepository $repo){

        $manager->remove($article);
        $manager->flush();

        return $this->redirectToRoute('blog_articles');
    }

}
